import Api from '@/plugins/Api'

export default {
  namespaced: true,
  state: {
    tokenFirebase: ''
  },
  mutations: {
    SET_TOKEN_FIREBASE (state, data) {
      state.tokenFirebase = data
    }
  },
  actions: {
    async pushNotification ({ commit }, data) {
      const res = await Api('https://fcm.googleapis.com/fcm/send').post(data)

      return res
    }
  },
  getters: {
    tokenFirebase (state) {
      return state.tokenFirebase
    },
    prayTime () {
      const result = [
        {
          img: require('@/assets/img/pray/subuh.svg'),
          imgDisabled: require('@/assets/img/pray/subuh-disabled.svg'),
          name: 'Subuh',
          time: '04:26'
        },
        {
          img: require('@/assets/img/pray/terbit.svg'),
          imgDisabled: require('@/assets/img/pray/terbit-disabled.svg'),
          name: 'Terbit',
          time: '05:43'
        },
        {
          img: require('@/assets/img/pray/dzuhur.svg'),
          imgDisabled: require('@/assets/img/pray/dzuhur-disabled.svg'),
          name: 'Dzuhur',
          time: '11:28'
        },
        {
          img: require('@/assets/img/pray/ashar.svg'),
          imgDisabled: require('@/assets/img/pray/ashar-disabled.svg'),
          name: 'Ashar',
          time: '14:57'
        },
        {
          img: require('@/assets/img/pray/maghrib.svg'),
          imgDisabled: require('@/assets/img/pray/maghrib-disabled.svg'),
          name: 'Maghrib',
          time: '17:49'
        },
        {
          img: require('@/assets/img/pray/isya.svg'),
          imgDisabled: require('@/assets/img/pray/isya-disabled.svg'),
          name: 'Isya',
          time: '18:58'
        }
      ]

      return result
    },
    menu () {
      const result = [
        {
          img: require('@/assets/img/menu/home.svg'),
          name: 'Home'
        },
        {
          img: require('@/assets/img/menu/sholat.svg'),
          name: 'Sholat'
        },
        {
          img: require('@/assets/img/menu/quran.svg'),
          name: 'Quran'
        },
        {
          img: require('@/assets/img/menu/profile.svg'),
          name: 'Profile'
        }
      ]

      return result
    }
  }
}
