import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import '@/assets/style/index.scss'
import '@/plugins/firebase'

Vue.config.productionTip = false

// vue-moment
const moment = require('moment')
require('moment/locale/id')
Vue.use(require('vue-moment'), {
  moment
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
