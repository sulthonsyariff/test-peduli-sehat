/* eslint-disable no-console */

import * as firebase from 'firebase/app'
import 'firebase/messaging'

var config = {
  apiKey: 'AIzaSyBoNwqx3BlyaotXLp7WE4A4RzAUcRaHt7k',
  authDomain: 'push-notification-98243.firebaseapp.com',
  projectId: 'push-notification-98243',
  storageBucket: 'push-notification-98243.appspot.com',
  messagingSenderId: '183099511950',
  appId: '1:183099511950:web:df3556a7e68d9c2e8665df',
  measurementId: 'G-0ERCNTKSWQ'
}

firebase.initializeApp(config)

const messaging = firebase.messaging()

messaging.usePublicVapidKey('BNSaLSz-7G8PlzenqMOSUsH1NLS9-J90pr7DPO3lJbwsHyu_KvovPg3-uTe69NfS5WmGfNyQNMbP7OjR439JRVA')

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function () {
    navigator.serviceWorker.register('firebase-messaging-sw.js', { scope: 'firebase-cloud-messaging-push-scope' })
      .then(function (registration) {
        messaging.useServiceWorker(registration)
        // messaging.getToken().then((token) => {
        //   localStorage.setItem('finaconsult-cfp-fcm-token', token)
        // })
        console.log('FCM service worker registered.')
      }).catch(function (err) {
        console.log('FCM service worker registration failed:', err)
      })
  })
}
