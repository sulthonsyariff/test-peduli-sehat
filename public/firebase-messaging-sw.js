/* eslint-disable no-undef */
/* eslint-disable no-console */

importScripts('https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/7.15.5/firebase-messaging.js')

firebase.initializeApp({
  apiKey: 'AIzaSyBoNwqx3BlyaotXLp7WE4A4RzAUcRaHt7k',
  authDomain: 'push-notification-98243.firebaseapp.com',
  projectId: 'push-notification-98243',
  storageBucket: 'push-notification-98243.appspot.com',
  messagingSenderId: '183099511950',
  appId: '1:183099511950:web:df3556a7e68d9c2e8665df',
  measurementId: 'G-0ERCNTKSWQ'
})

const messaging = firebase.messaging()

messaging.setBackgroundMessageHandler(function (payload) {
  const { title, body } = payload.notification
  const notificationTitle = title
  const notificationOptions = {
    body,
    icon: 'public/assets/logo.png'
  }

  return self.registration.showNotification(notificationTitle,
    notificationOptions)
})
