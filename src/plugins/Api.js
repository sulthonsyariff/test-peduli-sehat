// Provide axios instance to use same configuration across the whole project
import axios from 'axios'

export default (resource) => {
  const instance = axios.create()
  // instance.defaults.headers.Accept = 'application/json'
  instance.defaults.headers['Content-Type'] = 'application/json'
  // firebase server key
  instance.defaults.headers.Authorization = `key=${process.env.VUE_APP_FIREBASE_KEY}`
  instance.interceptors.response.use((response) => {
    return Promise.resolve(response.data)
  })

  return {
    instance,

    get (payload) {
      return instance.get(`${resource}`, {
        params: payload
      })
    },

    put (payload) {
      return instance.put(`${resource}`, payload)
    },

    post (payload) {
      return instance.post(`${resource}`, payload)
    },

    delete () {
      return instance.delete(`${resource}`)
    }

  }
}
